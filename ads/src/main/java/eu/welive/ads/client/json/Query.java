package eu.welive.ads.client.json;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

public class Query {
/**
 * {
  "from": 0,
  "size": 100,
  "query": {
    "filtered": {
      "query": {
        "query_string": {
          "query": "custom_CityName:\"Bilbao\"",
          "allow_leading_wildcard": false
        }
      },
      "filter": {
        "bool": {
          "must": {
            "range": {
              "timestamp": {
                "from": "1970-01-01 00:00:00.000",
                "to": "2016-02-03 14:15:01.099",
                "include_lower": true,
                "include_upper": true
              }
            }
          }
        }
      }
    }
  },
  "sort": [
    {
      "timestamp": {
        "order": "desc"
      }
    }
  ]
}
 */
	private int _from =0;
	private int _size = 1000;
	private String _query ="";
	private boolean _allow_leading_wildcard = false;
	private String _timefrom = "1970-01-01 00:00:00.000";
	private String _timeto = "2016-02-05 11:34:37.527";
	private String _order ="desc"; 
	public Query(int from, int size, String query, boolean _allow_leading_wildcard,String order ) {
		super();
	}
	public Query(String query ) {
		super();
		this._query = query;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date now = new Date();
		this._timeto = dateFormat.format(now);
	}	
	
	public JSONObject getQuery(){
		JSONObject query = new JSONObject();

		query.put("from",  _from);
		query.put("size",  _size);
		JSONObject subquery1 = new JSONObject();		
		JSONObject filtered = new JSONObject();
		JSONObject subquery2 = new JSONObject();
		JSONObject queryString = new JSONObject();
		JSONObject filter = new JSONObject();
		JSONObject bool = new JSONObject();
		JSONObject must = new JSONObject();
		JSONObject range = new JSONObject();
		JSONObject timestamp = new JSONObject();		
		JSONArray sortlist = new JSONArray();
		timestamp.put("from",_timefrom);
		timestamp.put("to",_timeto);
		timestamp.put("include_lower",true);
		timestamp.put("include_upper",true);
		range.put("timestamp", timestamp);
		must.put("range", range);
		bool.put("must", must);
		queryString.put("query", _query);
		queryString.put("allow_leading_wildcard", _allow_leading_wildcard);
		subquery2.put("query_string", queryString);
		filtered.put("query", subquery2);
		filter.put("bool", bool);		
		filtered.put("filter", filter);
		subquery1.put("filtered", filtered);
		query.put("query", subquery1);
		JSONObject sortitem = new JSONObject();
		JSONObject sorttimestamp = new JSONObject();
		
		sorttimestamp.put("order",_order);
		sortitem.put("timestamp", sorttimestamp);
		sortlist.put(sortitem);
		query.put("sort", sortlist);
		
		return query;
		
	}
	public String toString(){
		return getQuery().toString();
	}
}
