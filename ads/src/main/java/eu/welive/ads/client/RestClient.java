package eu.welive.ads.client;

import java.util.Hashtable;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Configuration;
import javax.net.ssl.*;

import org.glassfish.jersey.client.ClientConfig;




import eu.welive.ads.Authenticator;
import eu.welive.ads.client.json.Query;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class RestClient {
	
	private String _baseUrl ="";
	private String user ="";
	private String pass =""; 
	
	private final static Logger logger = Logger.getLogger(RestClient.class.getName());
	
	public RestClient() {
		_baseUrl = RestClientDefs.BASE_URL;
		user= "welive@welive.eu";
		pass ="w3l1v3t00ls";
		 //logger.info("_baseUrl: "+_baseUrl);
		 System.out.println("_baseUrl: "+_baseUrl);
	}
	public RestClient(String user, String pass) {
		_baseUrl = RestClientDefs.BASE_URL;
		this.user= user;
		this.pass =pass;
		 //logger.info("_baseUrl: "+_baseUrl);
		 System.out.println("_baseUrl: "+_baseUrl);
	}
	
	public Client initClient(Configuration config)  {
		SSLContext ctx;
		try {
			ctx = SSLContext.getInstance("SSL");
	        
			ctx.init(null, certs, new SecureRandom());

		    return ClientBuilder.newBuilder()
		                .withConfig(config)
		                .hostnameVerifier(new TrustAllHostNameVerifier())
		                .sslContext(ctx)
		                .build();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		return null;
	    
	}

	TrustManager[] certs = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }
            }
    };

    public static class TrustAllHostNameVerifier implements HostnameVerifier {

        public boolean verify(String hostname, SSLSession session) {
            return true;
        }

    }	

	private String Post(String sUrl, Hashtable<String, ?> hData) {
		
		
		String aux="";
		if (hData!=null) aux=hData.toString();
		//logger.info("Post: "+sUrl+" Data: "+aux);

		return Post(sUrl,  aux);
		
		
	}
	private String Post(String sUrl, String hData) {
		
		Client client = null;
		//logger.info("Post: "+sUrl+" Data: "+aux);
		System.out.println("Post: "+_baseUrl+sUrl+" Data: "+hData);
		if (_baseUrl.startsWith("https")){
			client =initClient(new ClientConfig());				 
		}
		else{
			client = ClientBuilder.newClient();
		}
		
		if (user!=null && !user.equals("") && pass!=null && !pass.equals("")){
			System.out.println("user:"+user+" Pass:"+pass);
			client.register(new Authenticator(user, pass));				
		}
		WebTarget target = client.target(_baseUrl);
		Response r = target.path(sUrl).request(MediaType.APPLICATION_JSON_TYPE).post(
				Entity.entity(hData, MediaType.APPLICATION_JSON_TYPE));

		//int status = r.getStatus();
		System.out.println(r.getStatus());
		
		return r.readEntity(String.class);
		
	}	
	public String getKPI1_1(String pilotid){
		_baseUrl = RestClientDefs.BASE_URL;
		return Post(RestClientDefs.KPI1_1+pilotid,"");
	}
	public String getKPI4_3(String pilotid){
		_baseUrl = RestClientDefs.BASE_URL;
		return Post(RestClientDefs.KPI4_3+pilotid,"");
	}
	public String getKPI11_1(String pilotid){
		_baseUrl = RestClientDefs.BASE_URL;
		return Post(RestClientDefs.KPI11_1+pilotid,"");
	}
	public String getKPI11_2(String pilotid){
		_baseUrl = RestClientDefs.BASE_URL;
		return Post(RestClientDefs.KPI11_2+pilotid,"");
	}
	public String getQueryFromLoggin(String strquery){
		_baseUrl = RestClientDefs.BASE_URL;
		Query query= new Query(strquery);
		
		
		
		return Post(RestClientDefs.WELIVE_LOGGIN_QUERY,query.toString());
		
	}
	public void login(String username, String password){
		Hashtable<String, String> hData = new Hashtable<String, String>();
		hData.put("username", username);
		hData.put("password", password);
	
		
		this.Post(RestClientDefs.URL_WELIVE_LOGIN, hData);
	}

}

