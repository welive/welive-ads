package eu.welive.ads;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import eu.welive.ads.client.RestClient;

/**
 * Root resource
 */
@Path("/")
public class ADSRestfulAPI {

    /**
     * Really for this KPI we only need to call to the CDV service that has the information.
     * Number of Users registered in the OIA (AAC or CDV profiles) who created or collaborated any need/idea (per city)
     * @return String in JSON Format with the stakeholders per city, {"bilbao":0,"helsinki":0,"novisad":0,"trento":1}
     */
	@GET
	@Path("/cdv/stakeholders")    
    @Produces(MediaType.APPLICATION_JSON)
    public String getKPI1_1() {
		RestClient rc = new RestClient();		
        return rc.getKPI1_1("all");
    }
	/**
     * Really for this KPI we only need to call to the CDV service that has the information.
     * Number of Users registered in the OIA (AAC or CDV profiles) who created or collaborated any need/idea (per city)
     * 
	 * @param pilotid : all, trento, bilbao, novisad, helsinki
	 * @return String in JSON Format with the stakeholders per city, {"bilbao":0}
	 */
	@GET
	@Path("/cdv/stakeholders/{pilotid}")    
    @Produces(MediaType.APPLICATION_JSON)
    public String getKPI1_1(@PathParam("pilotid") String pilotid) {
		RestClient rc = new RestClient();
		
        return rc.getKPI1_1(pilotid);
    }	
	
	/**
     * Really for this KPI we only need to call to the CDV service that has the information.
     * Number of users (registered) (per city)
	 * @return String in JSON Format  {"bilbao":20,"helsinki":0,"novisad":0,"trento":5}
	 */	
	@GET
	@Path("/cdv/user/all/count")    
    @Produces(MediaType.APPLICATION_JSON)
    public String getKPI4_3() {
		RestClient rc = new RestClient();		
        return rc.getKPI4_3("all");
    }
	/**
	 * Really for this KPI we only need to call to the CDV service that has the information.
	 * Classification of user per Age range (per city). This information comes from CDV
	 * @return  String in JSON Format {"bilbao":{"<20":0,"20-40":1,"40-60":19,">60":0},"helsinki":{"<20":0,"20-40":0,"40-60":0,">60":0},....}
	 */
	@GET
	@Path("/cdv/user/ages/all/count")    
    @Produces(MediaType.APPLICATION_JSON)
    public String getKPI11_1() {
		RestClient rc = new RestClient();		
        return rc.getKPI11_1("all");
    }	
	
	/**
	 * Really for this KPI we only need to call to the CDV service that has the information.
	 * Classification of user per gender (per city). This information comes from CDV
	 * @return  String in JSON Format {"bilbao":{"male":20,"female":0},"helsinki":{"male":0,"female":0},"novisad":{"male":0,"female":0},"trento":{"male":5,"female":0}}
	 */	
	@GET
	@Path("/cdv/user/gender/all/count")    
    @Produces(MediaType.APPLICATION_JSON)
    public String getKPI11_2() {
		RestClient rc = new RestClient();		

        return rc.getKPI11_2("all");
    }
	/**
	 * Total number of datasets processed by the ODS (Total and also per city)
	 * 
	 * Make a query to logging interface and parse the result in oder to get the data in the following format.
	 * {"Bilbao":6,"Test organization":2,"hits":210,"Trento":13,"Helsinki-Uusimaa":18,"Bilbao City Council":171}
	 * 
	 * @return String in JSON Format {"Bilbao":6,"Test organization":2,"hits":210,"Trento":13,"Helsinki-Uusimaa":18,"Bilbao City Council":171}
	 */
	@GET
	@Path("/ods/dataset/all/count") 
    @Produces(MediaType.APPLICATION_JSON)
    public String getKPI2_1() {
		RestClient rc = new RestClient();		
		JSONObject resultquery = new JSONObject(rc.getQueryFromLoggin("DatasetID:\"*\" AND CityName:\"*\" AND type:\"DatasetPublished\""));
		
		JSONObject result = new JSONObject();		
		
		Map<String, Integer> datasetspercities = new HashMap<String, Integer>();
		JSONObject jsonhits = (JSONObject)resultquery.get("hits");
		int hits =  jsonhits.getInt("total");
		JSONArray jarrayhits =  jsonhits.getJSONArray("hits");
		result.put("hits",  hits);
		
		for (int jsonhith=0;jsonhith<jarrayhits.length(); jsonhith++){
			JSONObject source = (JSONObject) ((JSONObject)jarrayhits.get(jsonhith)).get("_source");
			String city = (String) source.get("custom_CityName");
			if (datasetspercities.containsKey(city)){
				Integer value = datasetspercities.get(city);
				value = value + 1;
				datasetspercities.put(city,value);
				
			}
			else{
				datasetspercities.put(city,1);
			}
		}
		
		for(String key: datasetspercities.keySet()){
			
			result.put(key , datasetspercities.get(key));
			        
		}
		
        return result.toString();
    }	
	/**
	 * Number of types of broad datasets processed by the ODS (Total per type of dataset and also per city)
	 * 
	 * Make a query to logging interface and parse the result in oder to get the data in the following format.
	 * {
			"Bilbao": {
				"Private Data": 1,
				"Open Data": 2
			},
			"Test organization": {
				"Open Data": 2
			},
			"Trento": {
				"Private Data": 13
			},
			"Helsinki-Uusimaa": {
				"Private Data": 15,
				"Open Data": 3
			},
			"Novi Sad": {
				"Private Data": 1
			},
			"Bilbao City Council": {
				"Private Data": 171
			}
		}
	 * 
	 * @return String in JSON Format 
	 */
	@GET
	@Path("/ods/dataset/type/all/count")///ads/ods/dataset/all/count    
    @Produces(MediaType.APPLICATION_JSON)
    public String getKPI2_2() {
		RestClient rc = new RestClient();		
		JSONObject resultquery = new JSONObject(rc.getQueryFromLoggin("DatasetID:\"*\" AND CityName:\"*\" AND type:\"DatasetPublished\" AND DatasetType:\"*\""));
		
		JSONObject result = new JSONObject();		
		Map<String, Map<String,Integer>> datasetcities = new HashMap <String, Map<String,Integer>>();
		JSONObject jsonhits = (JSONObject)resultquery.get("hits");	
		JSONArray jarrayhits =  jsonhits.getJSONArray("hits");
		for (int h=0;h<jarrayhits.length(); h++){
			JSONObject source = (JSONObject) ((JSONObject)jarrayhits.get(h)).get("_source");
			String city = (String) source.get("custom_CityName");
			String custom_Type = Utils.getString(source, "custom_DatasetType");
			if (custom_Type == null) Utils.getString(source, "custom_Type");
			if (custom_Type == null) continue;
			if (datasetcities.containsKey(city)){
				Map<String,Integer> citydatasettypes = datasetcities.get(city);
				boolean added = false;
				for(String key: citydatasettypes.keySet()){
					if (key.equals(custom_Type)){
						Integer value = citydatasettypes.get(key);
						value = value + 1;
						citydatasettypes.put(key,value);
						added = true;
					}
				}
				if (!added){
					citydatasettypes.put(custom_Type,1);
				}
				
			}
			else{
				Map<String,Integer> citydatasettypes = new HashMap<String,Integer>();
				citydatasettypes.put(custom_Type,1);
				datasetcities.put(city, citydatasettypes);
				
			}
		}
		for(String key: datasetcities.keySet()){
			Map<String,Integer> citydatasettypes = datasetcities.get(key);
			JSONObject jsondatsetcity = new JSONObject();
			for(String dataset: citydatasettypes.keySet()){
				
				jsondatsetcity.put(dataset , citydatasettypes.get(dataset));
				        
			}
			result.put(key , jsondatsetcity);
			        
		}
		
		
        return result.toString();
    }		
	
	/**
	 * Number of times a Dataset is used (Usage of datasets). Most used datasets
	 * Make a query to logging interface and parse the result in oder to get the data in the following format.
		{
			"api-test": 1,
			"Test validation": 45,
			"Helsinki Area Address Catalogue": 3,
			"Presupuesto de ingresos del Ayuntamiento de Bilbao de 2015": 93,
			"Population projection in the Metropolitan Area in 2015-2024": 1,
			"bilbao-twitter-topics": 14,
			....
		}
	 * 
	 * @return String in JSON Format 
	 */
	@GET
	@Path("/ods/dataset/usage/all/count")
    @Produces(MediaType.APPLICATION_JSON)
    public String getKPI2_3() {
		RestClient rc = new RestClient();		
		JSONObject resultquery = new JSONObject(rc.getQueryFromLoggin("DatasetID:\"*\" AND ( type:\"DatasetMetadataAccessed\" OR type:\"DatasetMetadataUpdated\")"));
		JSONObject result = new JSONObject();		
		
		Map<String, Integer> datasetsuses = new HashMap<String, Integer>();
		JSONObject jsonhits = (JSONObject)resultquery.get("hits");
		JSONArray jarrayhits =  jsonhits.getJSONArray("hits");
		
		for (int h=0;h<jarrayhits.length(); h++){
			JSONObject source = (JSONObject) ((JSONObject)jarrayhits.get(h)).get("_source");
			String dataset =  Utils.getString (source,"custom_datasetName");
			if (dataset == null) continue;
			if (datasetsuses.containsKey(dataset)){
				Integer value = datasetsuses.get(dataset);
				value = value + 1;
				datasetsuses.put(dataset,value);
				
			}
			else{
				datasetsuses.put(dataset,1);
			}
		}
		
		for(String key: datasetsuses.keySet()){
			
			result.put(key , datasetsuses.get(key));
			        
		}
		
       return result.toString();
    }	
	/**
	 * 
	 * @param query
	 * @return
	 */
	@GET
	@Path("/ods/logging/{query}")    
    @Produces(MediaType.APPLICATION_JSON)
    public String getLoggingQuery(@PathParam("query") String query) {
		RestClient rc = new RestClient();
		
        return rc.getQueryFromLoggin(query);
    }	
 
}
