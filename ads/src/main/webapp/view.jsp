<%
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
%>


<%@ page import="eu.welive.ads.*"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.json.*" %>

<%
ADSRestfulAPI client = new ADSRestfulAPI();

String kpi2_1 = client.getKPI2_1();
String kpi2_2 = client.getKPI2_2();
%>
<html>
  <head>
  
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
		<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>      
 <style type="text/css">
		.rTable { 
		width: 100%;
		display: table; 
		} 
		.rTableRow { display: table-row; } 
		.rTableHeading { display: table-header-group; } 
		.rTableBody { display: table-row-group; } 
		.rTableFoot { display: table-footer-group; } 
		.rTableCell, .rTableHead { display: table-cell; }
		</style>
      <script type="text/javascript">
      jQuery(document).ready(function() {
  	    var datakpi2_1 = new Object();
		datakpi2_1 = <%=kpi2_1%>;		
		var total = datakpi2_1.hits;
		var arrlabels = [ 'Number of Datasets'];
		var dataarraykpi2_1 = [];
		//datasetspercities.forEach(function(value, key, datasetspercities) {
		//	dataarraykpi2_1.push([key,value]);
		//	});		
		for (var key in datakpi2_1) {			
			if (key=='hits') continue;
			dataarraykpi2_1.push({ y: eval("\""+key+"\""), a: eval(datakpi2_1[key])});//[key,datakpi2_1[key]]);
		}     
   	  
      var m4 = new Morris.Bar({
          // ID of the element in which to draw the chart.
          element: 'stacked-chart',
          // Chart data records -- each entry in this array corresponds to a point on
          // the chart.
          data: [dataarraykpi2_1],
          xkey: 'y',
          ykeys: ['a'],
          labels: arrlabels,
          barColors: ['#1CAF9A'],
          lineWidth: '1px',
          fillOpacity: 0.8,
          smooth: true,
          //stacked: true,
          hideHover: true
     });
      m4.setData(dataarraykpi2_1);
      
		  
  		//////////////////
	    //KPI 2.2
	    //////////////////
	    
	    var datakpi2_2 = new Object();
		datakpi2_2 = <%=kpi2_2%>;		
		
		var cities = Object.keys(datakpi2_2);
		var alldatasettypes =[];
		for (var key in datakpi2_2) {
			var datasetstypes = datakpi2_2[key];
			for (var dkey in datasetstypes) {
				if (alldatasettypes.indexOf(dkey)== -1){
					alldatasettypes.push(dkey);
				}
			}
		}
		
      	var dataarraykpi2_2 = [];
      	//var arraytopush =[];
      	//for	(index = 0; index < alldatasettypes.length; index++) {
      		//arraytopush.push(alldatasettypes[index]);
      		
      	//}
      	//arraytopush.push({ role: 'annotation' });
      	//dataarraykpi2_2.push(arraytopush);
      	
      	for (index=0; index < cities.length;index++){
      		var city = cities[index];
      		var citydatsettypes = datakpi2_2[city];
      		var objecttopush = new Object();
      		objecttopush.city = city; //[city];//{ y: eval("\""+key+"\""), a: eval(datakpi2_3[key])}
      		for (indexallcities =0 ; indexallcities < alldatasettypes.length; indexallcities++){
      			var datasettype= alldatasettypes[indexallcities];
      			var citykeys = Object.keys(datakpi2_2[city]);
      			if (citykeys.indexOf(datasettype)== -1){
      				objecttopush[datasettype] =0;//(0);
      			}
      			else {
      				var auxnumtype = citydatsettypes[eval("\""+datasettype+"\"")];
      				objecttopush[datasettype] =auxnumtype;
      			}

      		}
      		//arraytopush.push ('');
      		dataarraykpi2_2.push(objecttopush);
      	}
	      var chartkpi2_2 = new Morris.Bar({
	          // ID of the element in which to draw the chart.
	          element: 'chartkpi2_2',
	          // Chart data records -- each entry in this array corresponds to a point on
	          // the chart.
	          data: [dataarraykpi2_2],
	          xkey: 'city',
	          ykeys: alldatasettypes,
	          labels: alldatasettypes,
	         // barColors: ['#D9534F','#1CAF9A','#428BCA'],
	          lineWidth: '1px',
	          fillOpacity: 0.8,
	          smooth: false,
	          xLabelAngle: 35,
	          stacked: true,
	          hideHover: true
	     });
	      chartkpi2_2.setData(dataarraykpi2_2);
      	
 		
		    
	
	        
      
      });
      	

		
    </script>
  </head>
  <body>
  <div class="rTable">  <!-- style="width: 450px; height: 250px;" -->
  <div class="rTableRow">
	   
	    <div id="stacked-chart"  style="float:left;width: 450px; height: 250px;" ></div>
	    <div id="chartkpi2_2"  style="float:left;width: 450px; height: 250px;" ></div>
	    
	</div>

   </div>
   
          
  </body>
</html>