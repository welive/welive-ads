package eu.welive.ads.client;

public class RestClientDefs {

	// URL definitions.
	public static final String URL_WELIVE_LOGIN = "welive.logging";
	public static final String BASE_URL = "https://dev.welive.eu";
	public static final String KPI1_1 = "/cdv/api/statistics/operation/kpi_1.1/";//{pilotID}";
	public static final String KPI4_3 = "/cdv/api/statistics/operation/kpi_4.3/";//{pilotID}
	public static final String KPI11_1 = "/cdv/api/statistics/operation/kpi_11.1/";//{referredPilot}
	public static final String KPI11_2 = "/cdv/api/statistics/operation/kpi_11.2/";//{referredPilot}
	public static final String WELIVE_LOGGIN_QUERY = "/welive.logging/log/aggregate";
}
