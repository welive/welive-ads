package eu.welive.ads;

import org.json.JSONObject;

public class Utils {

	public static String getString(JSONObject object, String key){
		if (object.has(key)){
			return object.getString(key);
		}
		else {
			System.out.println("Key:"+key+" Not found in object:"+object.toString());
			return null;
		}
	}

}
